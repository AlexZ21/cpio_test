#include <cpioarchive.h>
#include <args/args.h>
#include <iostream>

void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

std::vector<std::string> split(char delim, const std::string &str)
{
    std::vector<std::string> parts;
    size_t start = 0;
    size_t end = 0;
    while ((end = str.find(delim, start)) != std::string::npos) {
        std::string sstr = str.substr(start, end - start);
        trim(sstr);
        parts.push_back(sstr);
        start = end + 1;
    }
    std::string sstr = str.substr(start);
    trim(sstr);
    parts.push_back(sstr);
    return parts;
}

//! --create --files "assets/829971.jpg;  assets/14809844768002.jpg" --output "test.cpio"
//! --create --dir "assets"  assets/14809844768002.jpg" --output "test.cpio"
//! --extract "test.cpio" --files "assets/829971.jpg;  assets/14809844768002.jpg" --output "test"
//! --list "test.cpio"
int main(int argc, char **argv)
{
    args::ArgumentParser parser("This is a test cpio extracter.", std::string());
    args::HelpFlag helpArg(parser, "help", "Display this help menu", {'h', "help"});

    args::Flag createArg(parser, "create", "Create archive", {"create"});
    args::ValueFlag<std::string> extractArg(parser, "extract", "Extract files to directory", {"extract"});
    args::ValueFlag<std::string> listArg(parser, "list", "Display files in archive", {"list"});

    args::ValueFlag<std::string> filesArg(parser, "files", "Files", {"files"});
    args::ValueFlag<std::string> dirArg(parser, "dir", "Directory", {"dir"});

    args::ValueFlag<std::string> outputArg(parser, "output", "Output archive file or "
                                                             "directory for extracted files", {"output"});

    try {
        parser.ParseCLI(argc, argv);
    } catch (args::Completion e) {
        std::cout << e.what() << std::endl;
        return 0;
    } catch (args::Help) {
        std::cout << parser << std::endl;
        return 0;
    } catch (args::ParseError e) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    if ((!createArg.Matched() && !extractArg.Matched()  && !listArg.Matched()) ||
            (createArg.Matched() && extractArg.Matched() && listArg.Matched())) {
        std::cerr << parser;
        return 0;
    }

    if (listArg.Matched()) {
        CpioArchive archive;
        if (!archive.open(listArg.Get())) {
            std::cerr << "Archive not found";
            return 1;
        }
        std::cout << archive;
        return 0;
    }

    if (createArg.Matched()) {
        if ((!filesArg.Matched() && !dirArg.Matched()) ||
                (filesArg.Matched() && dirArg.Matched()) ||
                !outputArg.Matched()) {
            std::cerr << parser;
            return 0;
        }

        if (filesArg.Matched()) {
            // Files
            std::vector<std::string> files = split(';', filesArg.Get());
            if (!CpioArchive::create(outputArg.Get(), files)) {
                std::cerr << "Can't create archive";
                return 1;
            }

        } else {
            // Dir
            if (!CpioArchive::create(outputArg.Get(), dirArg.Get())) {
                std::cerr << "Can't create archive";
                return 1;
            }
        }

    } else {
        if (!filesArg.Matched() || !outputArg.Matched()) {
            std::cerr << parser;
            return 0;
        }

        CpioArchive archive;
        if (!archive.open(extractArg.Get())) {
            std::cerr << "Archive not found";
            return 1;
        }

        std::vector<std::string> files = split(';', filesArg.Get());
        for (const std::string &file : files) {
            if (archive.extract(file, outputArg.Get()))
                std::cout << "Extracted <" << file << ">" << std::endl;
        }

    }

    return 0;
}
