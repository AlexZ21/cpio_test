#include "cpioarchive.h"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <iterator>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

#define ENTRY_FIELD_SIZE 2

int mkpath(std::string s, mode_t mode) {
    size_t pre = 0;
    size_t pos = 0;
    std::string dir;
    int mdret = 0;
    if(s.back() != '/')
        s += '/';
    while ((pos = s.find_first_of('/',pre)) != std::string::npos){
        dir = s.substr(0,pos++);
        pre = pos;
        if (dir.size() == 0)
            continue;
        if((mdret = mkdir(dir.c_str(),mode)) && errno != EEXIST)
            return mdret;
    }
    return mdret;
}

std::vector<std::string> filesList(std::string dirPath)
{
    std::vector<std::string> files;
    std::vector<std::string> dirPaths;
    dirPaths.push_back(dirPath);

    while (!dirPaths.empty()) {
        std::string dp = dirPaths.front();
        dirPaths.erase(dirPaths.begin());

        DIR *dir = opendir(dp.c_str());
        if (!dir) {
            std::cerr << "Error opening directory <" << dp << ">" << std::endl;
            continue;
        }

        struct dirent *dirp;
        while ((dirp = readdir(dir)) != nullptr) {
            std::string fileName(dirp->d_name);
            std::string fullPath;
            fullPath.append(dp);
            if (dp.at(dp.size() - 1) != '/')
                fullPath.append("/");
            fullPath.append(fileName);

            struct stat st;
            stat(fullPath.c_str(), &st);
            if (S_ISDIR(st.st_mode) && fileName != "." && fileName != "..")
                dirPaths.push_back(fullPath);
            else
                files.push_back(fullPath);
        }
        delete dirp;
        closedir(dir);
    }

    return files;
}

CpioArchive::CpioArchive()
{

}

bool CpioArchive::open(const std::string &filePath)
{
    m_entries.clear();

    std::fstream ifs(filePath);
    if (!ifs.is_open())
        return false;

    m_filePath = filePath;

    // Read headers
    while (!ifs.eof()) {
        Entry entry;

        // Magic
        ifs.read((char*)&entry.magic, ENTRY_FIELD_SIZE);

        // The device and inode numbers from the disk
        ifs.read((char*)&entry.dev, ENTRY_FIELD_SIZE);
        ifs.read((char*)&entry.ino, ENTRY_FIELD_SIZE);

        // The mode specifies both the regular permissions and the file type
        ifs.read((char*)&entry.mode, ENTRY_FIELD_SIZE);

        // The numeric user id and group id of the owner
        ifs.read((char*)&entry.uid, ENTRY_FIELD_SIZE);
        ifs.read((char*)&entry.gid, ENTRY_FIELD_SIZE);

        // The number of links to this file
        ifs.read((char*)&entry.nlink, ENTRY_FIELD_SIZE);

        // For block special and character special entries, this field con-
        // tains the associated device number
        ifs.read((char*)&entry.rdev, ENTRY_FIELD_SIZE);

        // Modification time of the file, indicated as the number of seconds
        // since the start of the epoch
        ifs.read(&((char*)&entry.mtime)[2], ENTRY_FIELD_SIZE);
        ifs.read(&((char*)&entry.mtime)[0], ENTRY_FIELD_SIZE);

        // The number of bytes in the pathname that follows the header
        ifs.read((char*)&entry.namesize, ENTRY_FIELD_SIZE);

        // The size of the file
        ifs.read(&((char*)&entry.filesize)[2], ENTRY_FIELD_SIZE);
        ifs.read(&((char*)&entry.filesize)[0], ENTRY_FIELD_SIZE);


        // The pathname immediately follows the fixed header
        entry.fileName.resize(entry.namesize - 1);
        ifs.read(&entry.fileName[0], entry.namesize - 1);
        ifs.seekg(ifs.tellg() + 1 + entry.namesize % 2);

        // Set data offset with skip term null
        entry.fileDataOffset = ifs.tellg();// + 1;
        ifs.seekg(ifs.tellg() + entry.filesize + entry.filesize % 2);

        if (entry.fileName == "TRAILER!!!")
            break;

        m_entries.emplace(entry.fileName, entry);

        if (ifs.tellg() == -1)
            break;
    }

    return true;
}

void CpioArchive::close()
{
    m_entries.clear();
    m_filePath.clear();
}

std::vector<char> CpioArchive::read(const std::string &filePath)
{
    std::vector<char> fileData;

    Entry *entry = findEntry(filePath);
    if (!entry)
        return fileData;

    if (entry->mode & 0x4000) {
        std::cerr << "Can't read directory";
        return fileData;
    }

    std::ifstream ifs(m_filePath);
    ifs.seekg(entry->fileDataOffset);
    fileData.resize(entry->filesize);
    ifs.read(&fileData[0], entry->filesize);

    return fileData;
}

bool CpioArchive::extract(const std::string &filePath, const std::string &outDir)
{
    Entry *entry = findEntry(filePath);
    if (!entry)
        return false;

    if (entry->mode & 0x4000) {
        std::cerr << "Can't extract directory";
        return false;
    }

    size_t p = filePath.find_last_of("/");
    std::string fileName = outDir + "/" + (p == std::string::npos ? filePath : filePath.substr(p + 1));

    std::ifstream ifs(m_filePath);
    if (!ifs.is_open())
        return false;
    ifs.seekg(entry->fileDataOffset);

    if (outDir != ".")
        mkpath(outDir, 0755);
    std::ofstream ofs(fileName, std::ios::out | std::ios::binary);
    if (!ofs.is_open())
        return false;

    int32_t currentpos = 0;
    int32_t maxCanRead = 1024;
    while (currentpos < entry->filesize) {
        size_t bufSize = ((entry->filesize - currentpos) < maxCanRead) ? (entry->filesize - currentpos) : maxCanRead;
        char buf[bufSize];
        ifs.read(buf, bufSize);
        ofs.write(buf, bufSize);
        currentpos += bufSize;
    }

    return true;
}

bool CpioArchive::create(const std::string &filePath, const std::vector<std::string> &files)
{
    if (files.empty())
        return false;

    size_t p = filePath.find_last_of("/");
    if (p != std::string::npos) {
        std::string dirPath = filePath.substr(0, p);
        if (!dirPath.empty())
            mkpath(dirPath, 0755);
    }

    std::ofstream ofs(filePath);
    if (!ofs.is_open())
        return false;

    std::vector<Entry> entries;

    for (const std::string &file : files) {
        struct stat fileStat;

        if (stat(file.c_str(), &fileStat) < 0 )
            continue;

        std::ifstream fileifs(file);
        if (!fileifs.is_open())
            continue;

        Entry entry;
        entry.magic = 29127;
        entry.dev = fileStat.st_dev;
        entry.ino = fileStat.st_ino;
        entry.mode = fileStat.st_mode;
        entry.uid = fileStat.st_uid;
        entry.gid = fileStat.st_gid;
        entry.nlink = fileStat.st_nlink;
        entry.rdev = fileStat.st_rdev;
        entry.mtime = fileStat.st_mtime;
        entry.namesize = file.size() + 1;
        entry.filesize = fileStat.st_size;
        entry.fileName = file;

        // Write file
        ofs.write((char*)&entry.magic, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.dev, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.ino, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.mode, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.uid, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.gid, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.nlink, ENTRY_FIELD_SIZE);
        ofs.write((char*)&entry.rdev, ENTRY_FIELD_SIZE);

        ofs.write(&((char*)&entry.mtime)[2], ENTRY_FIELD_SIZE);
        ofs.write(&((char*)&entry.mtime)[0], ENTRY_FIELD_SIZE);

        ofs.write((char*)&entry.namesize, ENTRY_FIELD_SIZE);

        ofs.write(&((char*)&entry.filesize)[2], ENTRY_FIELD_SIZE);
        ofs.write(&((char*)&entry.filesize)[0], ENTRY_FIELD_SIZE);

        const char *termnull = "\0";
        const char *nullc = "0";
        ofs.write(entry.fileName.data(), entry.fileName.size());
        ofs.write(termnull, 1);
        // Make even length
        if (entry.namesize % 2)
            ofs.write(nullc, 1);

        int32_t currentpos = 0;
        int32_t maxCanRead = 1024;
        while (currentpos < entry.filesize) {
            size_t bufSize = ((entry.filesize - currentpos) < maxCanRead) ? (entry.filesize - currentpos) : maxCanRead;
            char buf[bufSize];
            fileifs.read(buf, bufSize);
            ofs.write(buf, bufSize);
            currentpos += bufSize;
        }

        // Make even length
        if (entry.filesize % 2)
            ofs.write(nullc, 1);

        std::cout << "Add file <" << file << ">" << std::endl;
    }

    // TRAILER!!!
    writeTrailer(ofs);

    return true;
}

bool CpioArchive::create(const std::string &filePath, const std::string &dirPath)
{
    return create(filePath, filesList(dirPath));
}

CpioArchive::Entry *CpioArchive::findEntry(const std::string &filePath)
{
    auto it = m_entries.find(filePath);
    if (it == m_entries.end()) {
        std::cerr << "File <" << filePath << "> not found";
        return nullptr;
    }
    return &it->second;
}

void CpioArchive::writeTrailer(std::ostream &os)
{
    Entry entry;
    entry.magic = 29127;
    entry.fileName = "TRAILER!!!";
    entry.namesize = entry.fileName.size() + 1;

    os.write((char*)&entry.magic, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.dev, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.ino, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.mode, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.uid, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.gid, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.nlink, ENTRY_FIELD_SIZE);
    os.write((char*)&entry.rdev, ENTRY_FIELD_SIZE);

    os.write(&((char*)&entry.mtime)[2], ENTRY_FIELD_SIZE);
    os.write(&((char*)&entry.mtime)[0], ENTRY_FIELD_SIZE);

    os.write((char*)&entry.namesize, ENTRY_FIELD_SIZE);

    os.write(&((char*)&entry.filesize)[2], ENTRY_FIELD_SIZE);
    os.write(&((char*)&entry.filesize)[0], ENTRY_FIELD_SIZE);

    const char *termnull = "\0";
    const char *nullc = "0";
    os.write(entry.fileName.data(), entry.fileName.size());
    os.write(termnull, 1);
    // Make even length
    if (entry.namesize % 2)
        os.write(nullc, 1);
}


