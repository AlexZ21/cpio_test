# README #

Cpio archive extractor

# BUILD #
```
git clone https://AlexZ21@bitbucket.org/AlexZ21/cpio_test.git

cd cpio_test
mkdir build && cd build

cmake ..
make

cd test
./cpio_test_app

```
Output:
```
Add file <assets/14809844768002.jpg>
Add file <assets/..>
Add file <assets/692557.png>
Add file <assets/.>
Add file <assets/829971.jpg>
Add file <assets/dir1/..>
Add file <assets/dir1/341657.jpg>
Add file <assets/dir1/15155169935970.jpg>
Add file <assets/dir1/.>
Add file <assets/dir2/8767107509_98f314f176_h.jpg>
Add file <assets/dir2/..>
Add file <assets/dir2/.>
Add file <assets/dir2/6891861184_7da3d4ae91_z.jpg>
Add file <assets/cpio_test_src/..>
Add file <assets/cpio_test_src/.>
Add file <assets/cpio_test_src/src/..>
Add file <assets/cpio_test_src/src/.>
Add file <assets/cpio_test_src/src/cpioarchive.cpp>
Add file <assets/cpio_test_src/src/cpioarchive.h>
assets/cpio_test_src/src/cpioarchive.h
assets/cpio_test_src/src/cpioarchive.cpp
assets/cpio_test_src/src/.
assets/dir1/..
assets/dir2/8767107509_98f314f176_h.jpg
assets/829971.jpg
assets/14809844768002.jpg
assets/.
assets/..
assets/dir1/341657.jpg
assets/cpio_test_src/src/..
assets/692557.png
assets/dir1/15155169935970.jpg
assets/dir1/.
assets/dir2/..
assets/dir2/.
assets/cpio_test_src/..
assets/dir2/6891861184_7da3d4ae91_z.jpg
assets/cpio_test_src/.
```