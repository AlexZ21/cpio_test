#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>

class QStandardItemModel;
class QListView;

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    void createArchiveFromDirectory();
    void createArchiveFromFiles();

    void openArchive();

    void extractSelectedFiles();
    void extractAllFiles();

private:
    QStandardItemModel *m_entriesModel;
    QListView *m_entriesView;
    QString m_openedArchive;
};

#endif // MAINWINDOW_H
