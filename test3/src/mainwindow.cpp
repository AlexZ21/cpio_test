#include "mainwindow.h"

#include <cpioarchive.h>

#include <QVBoxLayout>
#include <QToolBar>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QFileDialog>
#include <QMessageBox>
#include <QStandardItemModel>
#include <QListView>
#include <QDebug>

#include <string>
#include <sstream>

std::vector<std::string> toStdVectorString(const QStringList &list)
{
    std::vector<std::string> strings;
    for (const QString &s : list)
        strings.push_back(s.toStdString());
    return strings;
}

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    setLayout(mainLayout);

    QToolBar *toolBar = new QToolBar();
    mainLayout->addWidget(toolBar, 0, Qt::AlignTop);

    QPushButton *createArchiveButton = new QPushButton("Create archive");
    toolBar->addWidget(createArchiveButton);

    QMenu *createArchiveMenu = new QMenu();
    createArchiveButton->setMenu(createArchiveMenu);
    QAction *createArchiveFromDirectoryAction = createArchiveMenu->addAction("From directory", this, &MainWindow::createArchiveFromDirectory);
    QAction *createArchiveFromFilesAction = createArchiveMenu->addAction("From files", this, &MainWindow::createArchiveFromFiles);

    QPushButton *openArchiveButton = new QPushButton("Open archive");
    toolBar->addWidget(openArchiveButton);
    connect(openArchiveButton, &QPushButton::clicked, this, &MainWindow::openArchive);

    m_entriesModel = new QStandardItemModel();
    m_entriesView = new QListView();
    mainLayout->addWidget(m_entriesView, 1);
    m_entriesView->setModel(m_entriesModel);
    m_entriesView->setSelectionMode(QAbstractItemView::ExtendedSelection);

    QToolBar *extractToolBar = new QToolBar();
    mainLayout->addWidget(extractToolBar, 0);

    QPushButton *extractFilesButton = new QPushButton("Extract files");
    extractToolBar->addWidget(extractFilesButton);
    connect(extractFilesButton, &QPushButton::clicked, this, &MainWindow::extractSelectedFiles);

    QPushButton *extractAllFilesButton = new QPushButton("Extract all files");
    extractToolBar->addWidget(extractAllFilesButton);
    connect(extractAllFilesButton, &QPushButton::clicked, this, &MainWindow::extractAllFiles);

    setWindowTitle("Cpio extractor");
}

void MainWindow::createArchiveFromDirectory()
{
    QString directoryPath = QFileDialog::getExistingDirectory(this, "Select input directory", QDir::currentPath());
    if (directoryPath.isEmpty()) {
        QMessageBox::warning(this, "Error", "Directory not selected");
        return;
    }

    QString outputFilesPath = QFileDialog::getSaveFileName(this, "Save file", QDir::currentPath(), "Archive (*.cpio)");
    if (outputFilesPath.isEmpty()) {
        QMessageBox::warning(this, "Error", "Otput file path is empty");
        return;
    }

    if (!CpioArchive::create(outputFilesPath.toStdString(), directoryPath.toStdString())) {
        QMessageBox::warning(this, "Error", "Archive not created");
    } else {
        QMessageBox::information(this, "Success", "Success creating archive");
    }
}

void MainWindow::createArchiveFromFiles()
{
    QStringList files = QFileDialog::getOpenFileNames(this, "Select files", QDir::currentPath());
    if (files.isEmpty()) {
        QMessageBox::warning(this, "Error", "Files not selected");
        return;
    }

    QString outputFilesPath = QFileDialog::getSaveFileName(this, "Save file", QDir::currentPath(), "Archive (*.cpio)");
    if (outputFilesPath.isEmpty()) {
        QMessageBox::warning(this, "Error", "Otput file path is empty");
        return;
    }

    if (!CpioArchive::create(outputFilesPath.toStdString(), toStdVectorString(files))) {
        QMessageBox::warning(this, "Error", "Archive not created");
    } else {
        QMessageBox::information(this, "Success", "Success creating archive");
    }

}

void MainWindow::openArchive()
{
    m_openedArchive = QFileDialog::getOpenFileName(this, "Open archive", QDir::currentPath(), "Archive (*.cpio)");
    if (m_openedArchive.isEmpty()) {
        QMessageBox::warning(this, "Error", "Archive file not selected");
        return;
    }

    CpioArchive archive;
    if (!archive.open(m_openedArchive.toStdString())) {
        QMessageBox::warning(this, "Error", "Can't open archive");
        return;
    }

    std::stringstream ss;
    ss << archive;
    QString filesStr = QString::fromStdString(ss.str());
    QStringList files = filesStr.split('\n');

    m_entriesModel->clear();
    for (const QString &file : files) {
        if (!file.isEmpty())
            m_entriesModel->appendRow(new QStandardItem(file));
    }
}

void MainWindow::extractSelectedFiles()
{
    if (m_openedArchive.isEmpty())
        return;

    QString outputDirPath = QFileDialog::getExistingDirectory(this, "Select output directory", QDir::currentPath());
    if (outputDirPath.isEmpty()) {
        QMessageBox::warning(this, "Error", "Otput directory path is empty");
        return;
    }

    CpioArchive archive;
    if (!archive.open(m_openedArchive.toStdString())) {
        QMessageBox::warning(this, "Error", "Can't open archive");
        return;
    }

    QModelIndexList indexes = m_entriesView->selectionModel()->selectedIndexes();
    if (indexes.isEmpty()) {
        QMessageBox::warning(this, "Error", "Files not selected");
        return;
    }

    uint32_t count = 0;
    for (const QModelIndex &index : indexes) {
        if (archive.extract(index.data().toString().toStdString(), outputDirPath.toStdString()))
            ++count;
    }

    QMessageBox::information(this, "Success", QString::number(count) + "files extracted");
}

void MainWindow::extractAllFiles()
{
    if (m_openedArchive.isEmpty())
        return;

    QString outputDirPath = QFileDialog::getExistingDirectory(this, "Select output directory", QDir::currentPath());
    if (outputDirPath.isEmpty()) {
        QMessageBox::warning(this, "Error", "Otput directory path is empty");
        return;
    }

    CpioArchive archive;
    if (!archive.open(m_openedArchive.toStdString())) {
        QMessageBox::warning(this, "Error", "Can't open archive");
        return;
    }

    if (m_entriesModel->rowCount() == 0) {
        QMessageBox::warning(this, "Error", "No files");
        return;
    }

    QModelIndex index = m_entriesModel->index(0, 0);
    uint32_t count = 0;
    while (index.isValid()) {
        if (archive.extract(index.data().toString().toStdString(), outputDirPath.toStdString()))
            ++count;
        index = index.sibling(index.row() + 1, 0);
    }

    QMessageBox::information(this, "Success", QString::number(count) + "files extracted");
}
