#ifndef CPIOARCHIVE_H
#define CPIOARCHIVE_H

#include <string>
#include <stdint.h>
#include <unordered_map>
#include <vector>
#include <iostream>

#define DECL_EXPORT __attribute__((visibility("default")))
#define DECL_IMPORT __attribute__((visibility("default")))
#define DECL_HIDDEN __attribute__((visibility("hidden")))

#ifdef CPIO_TEST_SHARED
#  define CPIO_TEST_EXPORT DECL_EXPORT
#else
#  define CPIO_TEST_EXPORT DECL_IMPORT
#endif

class CPIO_TEST_EXPORT CpioArchive
{
private:
    struct Entry {
        uint16_t magic = 0;
        uint16_t dev = 0;
        uint16_t ino = 0;
        uint16_t mode = 0;
        uint16_t uid = 0;
        uint16_t gid = 0;
        uint16_t nlink = 0;
        uint16_t rdev = 0;
        uint32_t mtime = 0;
        uint16_t namesize = 0;
        uint32_t filesize = 0;
        std::string fileName;
        size_t fileDataOffset = 0;
    };

public:
    CpioArchive();
    ~CpioArchive() = default;

    bool open(const std::string &filePath);
    void close();

    std::vector<char> read(const std::string &filePath);
    bool extract(const std::string &filePath, const std::string &outDir = ".");

    static bool create(const std::string &filePath,
                       const std::vector<std::string> &files);
    static bool create(const std::string &filePath, const std::string &dirPath);

    friend std::ostream &operator << (std::ostream &ostream, const CpioArchive &archive);

private:
    Entry *findEntry(const std::string &filePath);
    static void writeTrailer(std::ostream &os);

private:
    std::string m_filePath;
    std::unordered_map<std::string, Entry> m_entries;

};

inline std::ostream &operator <<(std::ostream &ostream, const CpioArchive &archive)
{
    for (const auto &it : archive.m_entries)
        ostream << it.first << std::endl;
    return ostream;
}

#endif // CPIOARCHIVE_H
