#include <cpioarchive.h>

#include <iostream>

int main()
{
    if (!CpioArchive::create("test/test.cpio", "assets")) {
        std::cerr << "Can't create archive";
        return 1;
    }

    CpioArchive archive;
    if (!archive.open("test/test.cpio")) {
       std::cerr << "Archive not found";
       return 1;
    }

    std::cout << archive;

    archive.extract("assets/692557.png", "extracted_files/imgs");
    archive.extract("assets/dir1/15155169935970.jpg", "extracted_files/imgs");
    archive.extract("assets/dir2/6891861184_7da3d4ae91_z.jpg", "extracted_files/imgs");
    archive.extract("assets/cpio_test_src/src/cpioarchive.cpp", "extracted_files");

    return 0;
}
